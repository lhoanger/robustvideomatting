#!/bin/bash

trap "kill 0" SIGINT # allow to kill all spawning processes in the same group

mkdir -p log/metrics

server_id=$1 # 1-based server id
num_server=$2
model_bb=$3 # model backbone {mobilenetv3, resnet50}
model_ckpt=$4 # path to model checkpoint
num_gpus=$(nvidia-smi --query-gpu=name --format=csv,noheader | wc -l)
commit_id=$(git rev-parse --verify HEAD)

# run id is model name without extension concatenated with git commit id
model_id="$(basename ${model_ckpt} | sed 's/\(.*\)\..*/\1/')"
run_id="${model_id}__${commit_id}"

test_set="$HOME/data/datasets/ov-official-segmentation-testset"
flavors=("train" "valid")

ov_bgrs="${test_set}/backgrounds/omnivor"
uw_bgrs="${test_set}/backgrounds/uw"

sample_uw_bgr=2 # pair each image/video with 2 random backgrounds from UW
sample_ov_bgr=1 # only 1 for omnivor green screen studio backgrounds

base_command="python inference_metric.py --variant ${model_bb} --checkpoint ${model_ckpt} --write-metrics"

# construct a list of images and videos to process
fgrs=()
phas=()
run_names=() # names for each run, used to write metric files
for image_set in `ls ${test_set}/images`; do
  for flv in "${flavors[@]}"; do
    fgrs+=("${test_set}/images/${image_set}/${flv}/fgr")
    phas+=("${test_set}/images/${image_set}/${flv}/pha")
    run_names+=("${image_set}__${flv}")
  done
done
for video_set in `ls ${test_set}/videos`; do
  for flv in "${flavors[@]}"; do
    for video in `ls ${test_set}/videos/${video_set}/${flv}/fgr`; do
      video_id="$(basename ${video} | sed 's/\(.*\)\..*/\1/')"
      fgrs+=("${test_set}/videos/${video_set}/${flv}/fgr/${video}")
      phas+=("${test_set}/videos/${video_set}/${flv}/pha/${video}")
      run_names+=("${video_set}__${flv}__${video_id}")
    done
  done
done

# generate a list of commands to execute
# skip commands that have already run before
uw_cmds=() # for UW backgrounds
ov_cmds=() # for omnivor green screen studio backgrounds
for (( i=0; i<${#fgrs[@]}; i++ ));
do
  cur_cmd="${base_command} --images-fgr ${fgrs[$i]} --images-pha ${phas[$i]}"
  uw_id="${run_id}__uw__${run_names[$i]}"
  if [ ! -f "./log/metrics/${uw_id}.csv" ]; then
    uw_cmds+=("${cur_cmd} --images-bgr ${uw_bgrs} --sample-bgr-rate ${sample_uw_bgr} --metric-id ${uw_id} > log/metrics/${run_id}__uw__${run_names[$i]}.log")
  fi
  ov_id="${run_id}__ov__${run_names[$i]}"
  if [ ! -f "./log/metrics/${ov_id}.csv" ]; then
    ov_cmds+=("${cur_cmd} --images-bgr ${ov_bgrs} --sample-bgr-rate ${sample_ov_bgr} --metric-id ${ov_id} > log/metrics/${run_id}__ov__${run_names[$i]}.log")
  fi
done
commands=("${ov_cmds[@]}" "${uw_cmds[@]}")

# determine which slice of the commands to process on this server
num_cmds=${#commands[@]}
num_cmds_per_server=$(( (num_cmds + num_server - 1) / num_server ))
start_idx=$(( (server_id - 1) * num_cmds_per_server ))
end_idx=$(( start_idx + num_cmds_per_server ))
if (( ${end_idx} > ${num_cmds} )); then
  end_idx=${num_cmds}
fi

echo "Generated ${num_cmds} commands, running slice [${start_idx}:${end_idx}]"

# generate a list of commands to be run on each gpu
base_bash_script="tmp_infer_metric"
for (( i=${start_idx}; i<${end_idx}; i+=${num_gpus} ));
do
  for (( j=0; j<${num_gpus}; j++ ));
  do
    cur_idx=$(( i + j ))
    if (( ${cur_idx} < ${end_idx} )); then
      final_command="CUDA_VISIBLE_DEVICES=${j} ${commands[$cur_idx]}"
      echo "${final_command}" >> "${base_bash_script}_${j}.sh"
    fi
  done
done

# actually run all the commands
# each gpu processes sequentially, but jobs on different gpus run in parallel
for (( j=0; j<${num_gpus}; j++ ));
do
  if [ -f "${base_bash_script}_${j}.sh" ]; then
    # -x prints out each executing command
    # & makes it run in background
    bash -x "${base_bash_script}_${j}.sh" &
  fi
done
wait

# clean up
rm ${base_bash_script}*.sh