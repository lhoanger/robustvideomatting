from torch.utils.data import Dataset
from typing import List
from tqdm import tqdm

class ZipDataset(Dataset):
  def __init__(self, datasets: List[Dataset], transforms=None, preload = False,
    assert_equal_length=False):

    self.datasets = datasets
    self.transforms = transforms
    
    if assert_equal_length:
      for i in range(1, len(datasets)):
        assert len(datasets[i]) == len(datasets[i - 1]), 'Datasets are not equal in length.'

    self.items = []
    if preload:
      print('Preloading zip datasets...')
      N = len(self)
      for idx in tqdm(range(N)):
        self.items.append(self.load(idx))
      print('Preloaded {} zip items'.format(N))
  
  def __len__(self):
    return max(len(d) for d in self.datasets)
  
  def __getitem__(self, idx):
    if idx < len(self.items):
      return self.items[idx]
    else:
      return self.load(idx)

  def load(self, idx):
    x = tuple(d[idx % len(d)] for d in self.datasets)
    if self.transforms:
      x = self.transforms(*x)
    return x
