from torch.utils.data import Dataset
from typing import List
import numpy as np

class KroneckerDataset(Dataset):
  '''
  Take two datasets A and B, return pairs (a,b) for every a in A and b in B.
  Iterates in the order (a1, b1), (a1, b2), ... (a2, b1), ...
  If a positive integer `sample_rate` is provided, for every a in A, 
  a random sample of that many from B is selected to pair with a.
  '''
  def __init__(self, datasets: List[Dataset],
    assert_equal_length=False, sample_rate = 0):

    assert len(datasets) == 2, \
      'Kronecker only supports 2 datasets, got {}'.format(len(datasets))
    if assert_equal_length:
      assert len(datasets[0]) == len(datasets[1]), \
        'Datasets are not equal in length.'

    self.datasets = datasets
    
    self.A = datasets[0]
    self.B = datasets[1]
    self.nA = len(datasets[0])
    
    # number of samples, ranging from 1 to count(B)
    nB = len(datasets[1])
    self.sB = nB if sample_rate <= 0 else min(sample_rate, nB)

    # for every item in A, pre-generate a list of random items from B
    self.iAToBList = []
    for _ in range(self.nA):
      iRand = np.arange(nB)
      np.random.shuffle(iRand)
      iRand = iRand[:self.sB]
      self.iAToBList.append(iRand)

    self.N = self.nA * self.sB
  
  def __len__(self):
    return self.N
  
  def __getitem__(self, idx):
    iA = idx // self.sB
    bList = self.iAToBList[iA] # get the random list of B items for this a
    bIndex = idx % self.sB # get the index into the random list
    iB = bList[bIndex] # get actual index in original dataset
    return (self.A[iA], self.B[iB], iA, iB)
