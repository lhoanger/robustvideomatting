import os
import glob
from torch.utils.data import Dataset
from PIL import Image, ImageOps
from tqdm import tqdm

class ImagesDataset(Dataset):
  def __init__(self, dirs, ext=None, mode='RGB', transforms=None, preload=False,
    resize = None):
    self.transforms = transforms
    self.mode = mode
    self.resize = resize
    if self.resize is not None:
      print(f'Resizing images to {self.resize}')
    self.filenames = []
    if type(dirs) != list:
      dirs = [dirs]
    for root in dirs:
      if ext is None:
        root_filenames = [
          *glob.glob(os.path.join(root, '**', '*.jpg'),  recursive=True),
          *glob.glob(os.path.join(root, '**', '*.jpeg'), recursive=True),
          *glob.glob(os.path.join(root, '**', '*.png'),  recursive=True)]
      else:
        root_filenames = glob.glob(os.path.join(root, '**', '*' + ext),
          recursive=True)
      self.filenames += root_filenames
      print('Found {} images from {}'.format(len(root_filenames), root))
    
    self.filenames = sorted(self.filenames)
    print('Found total {} images'.format(len(self.filenames)))

    self.images = []
    if preload:
      print('Preloading and transforming images...')
      for fn in tqdm(self.filenames):
        self.images.append(self.load_image(fn))
      print('Preloaded {} images'.format(len(self.images)))


  def __len__(self):
    return len(self.filenames)

  def __getitem__(self, idx):
    if idx < len(self.images):
      return self.images[idx]
    else:
      return self.load_image(self.filenames[idx])

  def get_item_name(self, idx):
    return '/'.join(self.filenames[idx].split('/')[-3:]) # short parent dir

  def load_image(self, fn):
    with Image.open(fn) as img:
      # PIL auto-rotates images based on EXIF tag
      # do this to keep it upright
      img = ImageOps.exif_transpose(img)
      img = img.convert(self.mode)
      if self.resize is not None:
        img = img.resize(self.resize)
      if self.transforms:
        img = self.transforms(img)
      return img
