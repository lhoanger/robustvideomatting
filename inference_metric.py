"""
Inference metric: Compute metric for trained models.

Example:

    python inference_metric.py \
        --variant mobilenetv3 \
        --checkpoint [...] \
        --downsample 0.25 \
        --input-resize 1920 1080 \
        --images-fgr "PATH_TO_IMAGES_FGR_DIR" \
        --images-pha "PATH_TO_IMAGES_PHA_DIR" \
        --images-bgr "PATH_TO_IMAGES_BGR_DIR"

"""

import kornia
import argparse
import sys, os
import datetime
import torch
from torch.nn import functional as F
from torch.utils.data import DataLoader
from torchvision import transforms as T
from torchvision.utils import make_grid, save_image
from tqdm import tqdm
from testing import ImagesDataset, VideoDataset, ZipDataset, KroneckerDataset
from testing import augmentation as A
import time
import cv2
import subprocess
from model import MattingNetwork


def get_git_revision_hash():
  return subprocess.check_output(['git', 'rev-parse', 'HEAD']).decode('ascii')\
    .strip()


def print_metrics(metrics, N):
  print('Total {} examples'.format(N))
  print('Average metric per example:')
  print('\n'.join(['{} = {:.5f}'.format(
    m, metrics[m] / N) for m in metrics]))


def update_metrics(metrics, pred_pha, true_pha,
  write_metrics, metric_id, video_name, image_name, bgr_name):
  loss_pha = F.l1_loss(pred_pha, true_pha)
  loss_sobel_pha = F.l1_loss(kornia.sobel(pred_pha), kornia.sobel(true_pha))

  metrics['alpha'] += loss_pha
  metrics['grad-alpha'] += loss_sobel_pha

  if write_metrics:
    with open(f'log/metrics/{metric_id}.csv', 'a') as omf:
      omf.write(
        f'{video_name},{image_name},{bgr_name},0,'
        f'{loss_pha},{loss_sobel_pha},0,0\n')


def main(arguments):
  global args
  parser = argparse.ArgumentParser(description='Inference Metric')

  parser.add_argument('--variant', type=str, required=True,
    choices=['mobilenetv3', 'resnet50'])
  parser.add_argument('--checkpoint', type=str, required=True)
  parser.add_argument('--downsample', type=float, default=0.25)
  parser.add_argument('--input-resize', type=int, default=[1920, 1080], nargs=2)
  parser.add_argument('--sample-bgr-rate', type=int, default=5, help='# of bgr '
    'images to sample and compose with each fgr image')
  parser.add_argument('--images-fgr', type=str, required=True)
  parser.add_argument('--images-pha', type=str, required=True)
  parser.add_argument('--images-bgr', type=str, required=True)

  parser.add_argument('--device', type=str, choices=['cpu', 'cuda'],
    default='cuda')
  parser.add_argument('--num-workers', type=int, default=0, 
      help='number of worker threads used in DataLoader. Note that Windows '
        'need to use single thread (0).')

  parser.add_argument('--write-metrics', action='store_true', 
    help='Whether to write metrics to csv file, the name of the output file '
      'is set as <metric-id>.csv')
  parser.add_argument('--metric-id', type=str, required=True,
    help='Used as name of the output metric file')
  parser.add_argument('--print-images', action='store_true')
  parser.add_argument('-y', action='store_true')

  args = parser.parse_args(arguments)
  print(args)

  device = torch.device(args.device)

  model = MattingNetwork(args.variant).eval().to(args.device)
  model.load_state_dict(torch.load(args.checkpoint, map_location=args.device))
  model = torch.jit.script(model)
  model = torch.jit.freeze(model)
  model = model.eval()

  img_datasets = []
  video_name = ''
  if os.path.isfile(args.images_pha) and os.path.isfile(args.images_fgr):
    img_datasets = [VideoDataset(args.images_pha, color = cv2.COLOR_BGR2GRAY),
                    VideoDataset(args.images_fgr)]
    video_name = '/'.join(args.images_pha.split('/')[-3:])
  elif os.path.isdir(args.images_pha) and os.path.isdir(args.images_fgr):
    img_datasets = [ImagesDataset(args.images_pha, mode='L', preload=True),
                    ImagesDataset(args.images_fgr, mode='RGB', preload=True)]
  else:
    print('Unsupported input, must specify fgr and pha for images or video')
    exit()

  bgr_dataset = ImagesDataset(args.images_bgr, mode='RGB', preload=True,
    transforms=T.Compose([T.Resize(args.input_resize[::-1]), T.ToTensor()]))

  # use Kronecker dataset to combine every foreground/alpha with a few or all
  # background images for testing to get diverse fg/bg context
  dataset = KroneckerDataset(
    datasets = [
      ZipDataset(
        datasets = img_datasets, 
        transforms=A.PairCompose([
          A.PairApply(T.Resize(args.input_resize[::-1])),
          A.PairApply(T.ToTensor())]), 
        assert_equal_length=True, 
        preload=True),
      bgr_dataset
    ], 
    sample_rate = args.sample_bgr_rate)
  dataloader = DataLoader(dataset, batch_size=1, \
    num_workers=args.num_workers, pin_memory=False)

  metrics = { k: 0 for k in [ 'loss', 'alpha', 'grad-alpha', 'fg', 'err', 
    'alfg' ] }
  nbatch = 0
  nexample = 0
  
  if args.print_images:
    run_dir = 'log/inference_metric_{}'.format(
      (datetime.datetime.utcnow() - datetime.datetime.utcfromtimestamp(0))\
        .total_seconds())
    os.makedirs(run_dir, exist_ok=True)
    print('Saving images to ' + run_dir)

  process_start = time.time()
  time_batch = 0
  time_model = 0
  time_compose = 0
  time_metric = 0
  time_load = 0
  load_start = time.time()

  rec = [None] * 4
  with torch.no_grad():
    for _, ((true_pha, true_fgr), true_bgr, i, j) in enumerate(tqdm(dataloader)):
      time_load += (time.time() - load_start)

      batch_start = time.time()
      true_pha = true_pha.to(device, non_blocking=True)
      true_fgr = true_fgr.to(device, non_blocking=True)
      true_bgr = true_bgr.to(device, non_blocking=True)
      true_src = true_fgr * true_pha + true_bgr * (1 - true_pha)

      time_compose += (time.time() - batch_start)

      model_start = time.time()
      fgr, pha, *recp = model(true_src, *rec, downsample_ratio=args.downsample)
      time_model += (time.time() - model_start)

      if len(video_name) > 0: # processing video
        rec = recp # update recurrent states for next video frame

      img_name = img_datasets[0].get_item_name(i.cpu().item())
      bgr_name = bgr_dataset.get_item_name(j.cpu().item())

      metric_start = time.time()
      update_metrics(metrics, pha, true_pha, args.write_metrics, args.metric_id,
        video_name, img_name, bgr_name)
      time_metric += (time.time() - metric_start)


      if args.print_images: # print images for diagnostics, very slow
        save_image(make_grid(true_src, nrow=4), 
          '{}/{:04d}_true_src.png'.format(run_dir, nbatch))
        save_image(make_grid(true_bgr, nrow=4), 
          '{}/{:04d}_true_bgr.png'.format(run_dir, nbatch))
        save_image(make_grid(fgr, nrow=4), 
          '{}/{:04d}_pred_fgr.png'.format(run_dir, nbatch))
        save_image(make_grid(pha, nrow=4), 
          '{}/{:04d}_pred_pha.png'.format(run_dir, nbatch))

      nbatch += 1
      nexample += true_bgr.size(0)
      time_batch += (time.time() - batch_start)
      load_start = time.time()
  
  assert nexample == len(dataset)
  print_metrics(metrics, nbatch)
  
  time_process = time.time() - process_start
  print(('Total Timing: process = {:.2f}s, within batch = {:.2f}s '
    '(load = {:.2f}s, compose = {:.2f}s, model = {:.2f}s, metric = {:.2f}s)')\
      .format(time_process, time_batch, time_load, time_compose, time_model,
        time_metric))
  

            
if __name__ == '__main__':
  sys.exit(main(sys.argv[1:]))
