import torch


if __name__ == '__main__':
  import argparse
  from model import MattingNetwork
  
  parser = argparse.ArgumentParser()
  parser.add_argument('--variant', type=str, required=True,
    choices=['mobilenetv3', 'resnet50'])
  parser.add_argument('--checkpoint', type=str, required=True)
  parser.add_argument('--output', type=str, required=True)
  parser.add_argument('--device', type=str, required=True)
  parser.add_argument('--precision', type=str, default='float32', 
    choices=['float32', 'float16'])
  parser.add_argument('--device', type=str, required=True, default='cuda')
  args = parser.parse_args()

  model = MattingNetwork(args.variant).eval().to(args.device)
  model.load_state_dict(torch.load(args.checkpoint, map_location=args.device))
  for p in model.parameters():
    p.requires_grad = False
  
  if args.precision == 'float16':
    model = model.half()

  model = torch.jit.script(model)
  model = torch.jit.freeze(model)
  model.save(args.output)
