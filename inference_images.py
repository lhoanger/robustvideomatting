import torch
from torch.utils.data import DataLoader
from torchvision.transforms.functional import to_pil_image
import os
import glob
from torch.utils.data import Dataset
from PIL import Image, ImageOps
from tqdm import tqdm
import numpy as np
from threading import Thread
import shutil
from torchvision import transforms as T


class ImagesDataset(Dataset):
  def __init__(self, dirs, ext=None, mode='RGB', transforms=None, preload=False,
    resize = None):
    self.transforms = transforms
    self.mode = mode
    self.resize = resize
    if self.resize is not None:
      print(f'Resizing images to {self.resize}')
    self.filenames = []
    if type(dirs) != list:
      dirs = [dirs]
    for root in dirs:
      if ext is None:
        root_filenames = [
          *glob.glob(os.path.join(root, '**', '*.jpg'),  recursive=True),
          *glob.glob(os.path.join(root, '**', '*.jpeg'), recursive=True),
          *glob.glob(os.path.join(root, '**', '*.png'),  recursive=True)]
      else:
        root_filenames = glob.glob(os.path.join(root, '**', '*' + ext),
          recursive=True)
      self.filenames += root_filenames
      print('Found {} images from {}'.format(len(root_filenames), root))
    
    self.filenames = sorted(self.filenames)
    print('Found total {} images'.format(len(self.filenames)))

    self.images = []
    if preload:
      print('Preloading and transforming images...')
      for fn in tqdm(self.filenames):
        self.images.append(self.load_image(fn))
      print('Preloaded {} images'.format(len(self.images)))


  def __len__(self):
    return len(self.filenames)

  def __getitem__(self, idx):
    if idx < len(self.images):
      return self.images[idx]
    else:
      return self.load_image(self.filenames[idx])

  def get_item_name(self, idx):
    return '/'.join(self.filenames[idx].split('/')[-3:]) # short parent dir

  def load_image(self, fn):
    with Image.open(fn) as img:
      # PIL auto-rotates images based on EXIF tag
      # do this to keep it upright
      img = ImageOps.exif_transpose(img)
      img = img.convert(self.mode)
      if self.resize is not None:
        img = img.resize(self.resize)
      if self.transforms:
        img = self.transforms(img)
      return img


def writer_alpha(img, src, path):
  img = to_pil_image(img[0].cpu())
  img.save(os.path.splitext(path)[0] + '_mask.png')
  np_src = np.array(to_pil_image(src.squeeze(0))).astype(float)
  np_img = np.array(img).astype(float)
  alpha = np.expand_dims(np_img / 255, axis = 2)
  fore = alpha * np_src
  fore = fore.astype(np.uint8)
  fore_img = Image.fromarray(fore)
  fore_img.save(os.path.splitext(path)[0] + '_fg.png')


if __name__ == '__main__':
  import argparse
  from model import MattingNetwork
  
  parser = argparse.ArgumentParser()
  parser.add_argument('--variant', type=str, required=True,
    choices=['mobilenetv3', 'resnet50'])
  parser.add_argument('--checkpoint', type=str, required=True)
  parser.add_argument('--device', type=str, required=True, default='cuda')
  parser.add_argument('--input-dir', type=str, required=True)
  parser.add_argument('--output-dir', type=str, required=True)
  parser.add_argument('--input-resize', type=int, default=None, nargs=2)
  parser.add_argument('--num-workers', type=int, default=0)
  parser.add_argument('--disable-progress', action='store_true')
  parser.add_argument('-y', action='store_true')
  args = parser.parse_args()

  if os.path.exists(args.output_dir):
    if args.y or input(f'Directory {args.output_dir} already exists. '
      'Override? [Y/N]: ').lower() == 'y':
      shutil.rmtree(args.output_dir)
    else:
      exit()
  os.makedirs(args.output_dir)

  model = MattingNetwork(args.variant).eval().to(args.device)
  model.load_state_dict(torch.load(args.checkpoint, map_location=args.device))
  model = torch.jit.script(model)
  model = torch.jit.freeze(model)
  model = model.eval()

  image_dataset = ImagesDataset(args.input_dir, '_img.png', 
    resize=args.input_resize, transforms=T.ToTensor())
  dataloader = DataLoader(image_dataset, batch_size=1, 
    num_workers=args.num_workers, pin_memory=True)

  # bgr = torch.tensor([.47, 1, .6]).view(3, 1, 1).cuda()
  rec = [None] * 4
  
  with torch.no_grad():
    for i, src in tqdm(enumerate(dataloader)):
      fgr, pha, *dummy = model(src.cuda(), *rec, downsample_ratio=0.25)

      pathname = os.path.basename(image_dataset.filenames[i])
      pathname = os.path.splitext(pathname)[0]
      Thread(target=writer_alpha, args=(pha, src, os.path.join(args.output_dir,
        pathname + '.png'))).start()